<?php
/* @var $this LanguageController */
/* @var $model Language */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('language/export'),
	'method'=>'get',
	'id'=>'language-export-form',

)); ?>


<?php echo CHtml::submitButton('Export Languages'); ?>
<?php $this->endWidget(); ?>


