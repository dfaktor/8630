<?php
/* @var $this LanguageController */
/* @var $model Language */
/* @var $form CActiveForm */
?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('language/simpleSearch'),
	'method'=>'get',
	'id'=>'language-form',

)); ?>

<tr class="odd"><td></td>
<div class="compactRadioGroup">
            <?php
                            echo CHtml::radioButtonList('lang', $lang,
                                            array(  'ru' => 'ru',
                                                    'en' => 'en',
                                            	    'fi' => 'fi',
                                            	    'de' => 'de',
                                            	    'fr' => 'fr',
                                            	    'se' => 'se',
                                            	    'es' => 'es',
                                            	    'rut' => 'rut'
                                            	     ),
                                            array( 'separator' => "",
                                        	    'template' => '<td>{input}</td>'
                                             )         
                                             );
            ?>
</div>
<td></td>
</tr>
<tr class="odd">
<td id="language-grid_c8" colspan="1"><?php echo $form->textField($model,'name',array('size'=>45,'maxlength'=>255)); ?></td>
<td id="language-grid_c9" colspan="8"><?php echo CHtml::textField('string', $string ,array('size'=>45));?></td>

<td><?php echo CHtml::submitButton('Submit'); ?>
<?php $this->endWidget(); ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('language/admin'),
	'method'=>'get',
)); ?>
<?php echo CHtml::submitButton('x'); ?>
</td></tr>
<?php $this->endWidget(); ?>

